package com.devcamp.task53;

public class Circle {
    //thuộc tính bán kinh
    public double radius = 1.0;

        //phương thức khởi tạo
    public Circle() {};

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    };

    //phương thức tính diện tích

    public double getArea(){
        return Math.pow(this.radius, 2)* Math.PI;
    }


    //phương thúc tính chu vi
    public double getCircumference(){
        return (this.radius*2)* Math.PI;
    }

    //in ra console log
    @Override
    public String toString(){
        return String.format("Circle [radius = %s]", radius);
    }
}
