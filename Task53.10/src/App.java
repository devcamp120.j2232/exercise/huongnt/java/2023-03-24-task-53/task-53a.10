import java.util.PrimitiveIterator.OfDouble;

import com.devcamp.task53.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        //khởi tạo đối tượng circle1 k tham số
        Circle circle1 = new Circle();
        //khởi tạo đối tượng circle 2 có tham số
        Circle circle2 = new Circle(3.0);
        System.out.println("Circle1");
        System.out.println(circle1.toString());
        System.out.println(circle1.getRadius());
        System.out.println(circle1.getArea());
        System.out.println(circle1.getCircumference());
        System.out.println("Circle2");
        System.out.println(circle2.toString());
        System.out.println(circle2.getRadius());
        System.out.println(circle2.getArea());
        System.out.println(circle2.getCircumference());

    }
}
